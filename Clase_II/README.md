 * #### Clase II. Python: Variables y control de flujo
	* Introducción a Python: Sintaxis general.
	    * [Bionformática en el aula](http://ufq.unq.edu.ar/sbg/files/Guia_Taller_Programacion_Biologia_2019.pdf) 
	* Listas, tuplas, sets.
		* [Sintáxis básica](https://bioinf.comav.upv.es/courses/linux/python/listas.html)
		* [Lista anidada](htttp://www.mclibre.org/consultar/python/lecciones/python-listas.html)
		* [Tuplas](https://datacarpentry.org/python-ecology-lesson-es/01-short-introduction-to-Python/index.html)
		* [Sets](https://bioinf.comav.upv.es/courses/linux/python/estructuras_de_datos.html#sets)
	* Control de flujo.
		* [condicionales: if, else, elif](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html)
		* [bucles: for](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html#bucle-for)
		* [bucles: while](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html#while)
	* [Diccionarios](https://bioinf.comav.upv.es/courses/linux/python/estructuras_de_datos.html#)
	* **Ejercicios** :arrow_down:
		* [Operaciones, input (ejercicio_0)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_0.md)
		* [Strings (ejercicio_1)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_1.md)
		* [Listas (ejercicio_2)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_2.md)
		* [Condicionales (ejercicio_3)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_3.md)
		* [Diccionario (ejercicio_4)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_4.md)
		* [Variados (ejercicio_5)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/ejercicio_5.md)
			* [soluciones ejercicio_5](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_II/soluciones_de_ejercicio_5.py)

